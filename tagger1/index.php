<?php

/* config here */
$sitename_di = 'haryana.cf'; //no www on start and no slash (/)
$default_mp3_directory =  "datas/"; //the directory your file will be stored
$sitename = "Haryana.cf"; //Will be added with file at the end of album, artist, name etc
$url = "http://haryana.cf"; //Complete url of your site
/*config end */

echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv=\'Content-Type\' content=\'text/html; charset=ISO-8859-1;charset=windows-1252\' />
<meta name="keywords" content="mp3 tag editor,php mp3 tag editor,automatic mp3 tag editor" />
<meta name="description" content="Edit mp3 tags online." />
<title>DamnIdea.com Online mp3 Tag Editor</title>
<link rel="stylesheet" href="style.css" />
</head>
<body>';

function friendly_size($size,$round=2)
{
$sizes=array(' Byts',' Kb',' Mb',' Gb',' Tb');
$total=count($sizes)-1;
for($i=0;$size>1024 && $i<$total;$i++){
$size/=1024;
}
return round($size,$round).$sizes[$i];
}
if(isset($_GET['site']))
{
$sitename= strip_tags($_GET['site']);
}
if(isset($_GET['url']))
{
$url = strip_tags($_GET['url']);
}
$default_filename_prefix = ' ('.$sitename.').mp3';
$default_songname_prefix = ' ('.$sitename.')';
$default_comment = 'Mp3 song download @ '.$sitename.'';
$default_artist = ' ('.$sitename.')';
$default_album = ' ('.$sitename.')';
$default_year = date("Y");
$default_genre = ''.$sitename.'';
?>
<header><a href="#"><img src="logo.png" alt="logo" title=""></a></header>
<?php
if(isset($_POST['submit'])){
$mp3_filepath = $_POST['mp3_filepath'];
$mp3_filename = $_POST['mp3_filename'];
$mp3_songname = $_POST['mp3_songname'];
$mp3_comment = $_POST['mp3_comment'];
$mp3_band = $_POST['mp3_band'];
$mp3_publisher = $_POST['mp3_publisher'];
$mp3_composer = $_POST['mp3_composer'];
$mp3_original_artist = $_POST['mp3_original_artist'];
$mp3_copyright = $_POST['mp3_copyright'];
$mp3_encoded_by = $_POST['mp3_encoded_by'];
$mp3_user_defined = $_POST['mp3_user_defined'];
$mp3_wxxx = $_POST['mp3_wxxx'];
$mp3_artist = $_POST['mp3_artist'];
$mp3_album = $_POST['mp3_album'];
$mp3_year = $_POST['mp3_year'];
$mp3_genre = $_POST['mp3_genre'];
$mp3_track = $_POST['track'];

if(filter_var($mp3_filepath,FILTER_VALIDATE_URL)){
if($mp3_filename!="")
{
$mp3_filename = str_replace(DIRECTORY_SEPARATOR,"-X-",$mp3_filename);
if(strtolower(@end(@explode(".",basename($mp3_filepath))))!="mp3"){
exit("<br />URL must have a .mp3 extension !");
}
if(strtolower(@end(@explode(".",basename($mp3_filename))))!="mp3"){
exit("<br />Filename must have a .mp3 exntension !");
}
 $sname = $default_mp3_directory.$mp3_filename;
 if((file_exists($sname)) || (file_exists("voicetracks/".$mp3_filename)))
 {
 $extension = @end(@explode('.',$sname));
 $mp3_filename = str_replace(".".$extension,"_".rand(0,999).".".$extension,$mp3_filename);   
 } 
 $sname = $default_mp3_directory.$mp3_filename;
  $j_sname = $default_mp3_directory.$mp3_filename;
 if(@$_POST['mp3_voice_track']=="on")
 {
 $sname = "voicetracks/".$mp3_filename;
 }
if(copy($mp3_filepath,$j_sname)){
if(@$_POST['mp3_voice_track']=="on" && $_POST['mp3_voicetrack']!='http://')
{
if(copy($_POST['mp3_voicetrack'],"temp_joins/".$mp3_filename))
{
include 'function.joinmp3.php';
$place = $_POST['mp3_voice_track_add'];
if($place=="first")
{
$FilenamesIn[] = "temp_joins/".$mp3_filename;
$FilenamesIn[] = $j_sname;
}
else
{
$FilenamesIn[] = $j_sname;
$FilenamesIn[] = "temp_joins/".$mp3_filename;
}
 if (CombineMultipleMP3sTo($sname, $FilenamesIn)) {
unlink("temp_joins/".$mp3_filename);
unlink($default_mp3_directory."/".$mp3_filename);
rename("voicetracks/".$mp3_filename,$default_mp3_directory."/".$mp3_filename);
 }
 }
 else echo '<div class="rmenu">Unable to upload Voice Track</div><br />';
}
$sname = $default_mp3_directory."/".$mp3_filename;
$size = friendly_size(filesize($sname));        
$mp3_tagformat = 'UTF-8';
require_once('getid3/getid3.php');
$mp3_handler = new getID3;
$mp3_handler->setOption(array('encoding'=>$mp3_tagformat));
require_once('getid3/write.php'); 
$mp3_writter = new getid3_writetags;
$mp3_writter->filename       = $sname;
$mp3_writter->tagformats     = array('id3v1', 'id3v2.3');
$mp3_writter->overwrite_tags = true;
$mp3_writter->tag_encoding   = $mp3_tagformat;
$mp3_writter->remove_other_tags = true;
$mp3_data['title'][]   = $mp3_songname;
$mp3_data['artist'][]  = $mp3_artist;
$mp3_data['album'][]   = $mp3_album;
$mp3_data['year'][]    = $mp3_year;
$mp3_data['genre'][]   = $mp3_genre;
$mp3_data['comment'][] = $mp3_comment;
$mp3_data['publisher'][] = $mp3_publisher;
$mp3_data['composer'][] = $mp3_composer;
$mp3_data['band'][] = $mp3_band;
$mp3_data['commercial_information'][] = $mp3_wxxx;
$mp3_data['url_user'][] = $mp3_user_defined;
$mp3_data['track'][] = $mp3_track;
$mp3_data['original_artist'][] = $mp3_original_artist;
$mp3_data['copyright_message'][] = $mp3_copyright;
$mp3_data['encoded_by'][] = $mp3_encoded_by;

$album_art = @end(@explode('/',$_POST['album_art']));
$album_path = "temp_albumarts/".$album_art;
$album_ext = @end(@explode('.',$album_art));
if(in_array($album_ext,array("png","jpeg","gif","jpg")) && (copy($_POST['album_art'],$album_path)))
{
if($album_ext=="jpeg" || $album_ext=="jpg") $type = "image/jpeg";
if($album_ext=="png") $type="image/png";
if($album_ext=="gif") $type="image/png";
$mp3_data['attached_picture'][0]['data'] = file_get_contents($album_path);
$mp3_data['attached_picture'][0]['picturetypeid'] = $type;
$mp3_data['attached_picture'][0]['description'] = $album_art;
$mp3_data['attached_picture'][0]['mime'] = $type;
}
else
{
echo '<div class="rmenu">Incompartible image !</div>'; 
}

$mp3_writter->tag_data = $mp3_data;

if($mp3_writter->WriteTags()) {
$id3 = new getID3;
$info = $id3->analyze($sname);
echo '<div class="subhead">MP3 Taged Notice</div><div class="wrapper content"><div class="form-grp">
<center><font color="green">Tags Were Successfully Written.</font></center>
</div><div class="form-grp">Copied:</div><div class="func">
'.$mp3_filepath.'</div><div class="form-grp">To:</div><br/><div class="form-grp"><center>
<a class="btn-dload" href="'.$sname.'">Download Now<br>'.basename($sname).'</a>
<br>Size: <font color="blue">( '.$size.' )</font></center></div>
<div class="subhead">Direct Download Url</div><div align="center" class="form-grp">
<input size="25" type="text" class="input" name="mp3_filename" value="'.$url.'/'.$sname.'"></div><div class="bmenu">
<font color="red">Warning:</font> Download Link Is AVAILABLE 1 hour</div>
';
if(@$_POST['mp3_info_view']=="on")
{
echo '<div class="subhead">Mp3 Tag Information
</div>';
if(!empty($_POST['album_art']))echo '<div class="label"><center><img class="gmenu" src="cover.php?type='.$type.'&p='.basename($sname).'&img='.$album_art.'" width="200" height="200" alt="art">
</center></div>';
echo '<div class="form-grp"><div class="label">Title: '.$info['id3v1']['title'].'</div><div class="label">Artist: '.$info['id3v1']['artist'].'</div>
<div class="label">Album: '.$info['id3v1']['album'].'</div>
<div class="label">Comment: '.$info['id3v1']['comment'].'</div><div class="label">Year: '.$info['id3v1']['year'].'</div>
<div class="label">Duration: '.$info['playtime_string'].'</div></div></div>';
}
}
else{
echo"<br />Failed to write tags!<br>".implode("<br /><br />",$mp3_writter->errors);
}
}
else{echo '<div class="rmenu">Unable to copy file.</div>';}
}
else{echo '<div class="rmenu">Empty filename.</div>';}
}
else{echo '<div class="rmenu">Invalid FilePath.</div>';}
}
else{
?>
<div class="subhead">Edit ID3 and ID2 Tag Online</div>
<div class="content wrapper">
<div class="form-header">Edit Tag</div>
<form method="post" action="" enctype="multipart/form-data">
<div class="form-grp"><div class="label">Mp3 url</div><input type="text" class="input" name="mp3_filepath" value="" /></div>
<div class="form-grp"><div class="label"> Album art/Cover</div><input  type="text" class="input" name="album_art" value="http://<?php echo $sitename_di; ?>/cover.jpg" /></div>
<div class="form-grp"><div class="label"> Filename</div><input  type="text" class="input" name="mp3_filename" value="<?php echo $default_filename_prefix ; ?>" /> </div>
<div class="form-grp"><div class="label">Song name/title</div><input type="text" class="input" name="mp3_songname" value="<?php echo $default_songname_prefix ; ?>" /></div>
<div class="form-grp"><div class="label">Album</div><input type="text" class="input" name="mp3_album" value="<?php echo $default_album ; ?>" /> </div>
<div class="form-grp"><div class="label">Artist(s)</div><input type="text" class="input" name="mp3_artist" value="<?php echo $default_artist ; ?>" /></div>
<div class="form-grp"><div class="label">Comment</div><input type="text" class="input" name="mp3_comment" value="<?php echo $default_comment ; ?>" /></div>
<div class="form-grp"><div class="label"> Band</div><input type="text" class="input" name="mp3_band" value="<?php echo $sitename; ?>" /></div>
<div class="form-grp"><div class="label"> Publisher</div><input type="text" class="input" name="mp3_publisher" value="<?php echo $url; ?>" /></div>
<div class="form-grp"><div class="label"> Composer</div><input type="text" class="input" name="mp3_composer" value="<?php echo $sitename; ?>" /></div>
<div class="form-grp"><div class="label">Original Artist</div><input type="text" class="input" name="mp3_original_artist" value="<?php echo $sitename; ?>" /></div>
<div class="form-grp"><div class="label">Copyright</div><input type="text" class="input" name="mp3_copyright" value="<?php echo $sitename; ?>" /></div>
<div class="form-grp"><div class="label">Encoded By</div><input type="text" class="input" name="mp3_encoded_by" value="<?php echo $sitename; ?>" /></div>
<div class="form-grp"><div class="label">User Defined Url</div><input type="text" class="input" name="mp3_user_defined" value="<?php echo $url; ?>" /></div>
<div class="form-grp"><div class="label">Commercial Url</div><input type="text" class="input" name="mp3_wxxx" value="<?php echo $url; ?>" /></div>
<div class="form-grp"><div class="label">Track Number</div><select name="track" class="input"><?php for($i=0;$i<=19;$i++) echo '<option value="'.$i.'">'.$i.'</option>'; ?></select></div> 
<div class="form-grp"><div class="label">Released Year</div><select class="input" name="mp3_year"><?php for($d=date('Y');$d>=2000;$d--) echo '<option value="'.$d.'">'.$d.'</option>'; ?> </select></div>
<div class="form-grp"> <div class="label"> Genre</div><select name="mp3_genre" class="input">
<option value="<?php echo $default_genre ; ?>"><?php echo $default_genre ; ?></option>
<option value="Blues">Blues</option>
<option value="Classical">Classical</option>
<option value="Classic Rock">Classic Rock</option>
<option value="Country">Country</option>
<option value="Dance">Dance</option>
<option value="Drum Solo">Drum Solo</option>
<option value="Fusion">Fusion</option>
<option value="Gangstar">Gangstar</option>
<option value="Hip-Hop">Hip-Hop</option>
<option value="Instrumental Rock">Instrumental Rock</option>
<option value="Instrumental PoP">Instrumental PoP</option>
<option value="Jazz">Jazz</option>
<option value="Metal">Metal</option>
<option value="Oldies">Oldies</option>
<option value="PoP">PoP</option>
<option value="PoP-Folk">PoP-Folk</option>
<option value="Porn Groove">Porn Groove</option>
<option value="R&amp;B">R&amp;B</option>
<option value="Symphony">Symphony</option>
<option value="Slow Rock">Slow Rock</option>
<option value="Symphonic Rock">Symphonic Rock</option>
<option value="Sound Track">Sound Track</option>
<option value="Other">Other</option>
</select></div>
<div class="form-grp"><div class="label"><input type="checkbox" name="mp3_voice_track" />Add Voice Track (Optional)</div></div>
<div class="form-grp"> Voice Track Url [<a href="http://fromtexttospeech.com/" target="blank">Make Track</a>]<br /><input size="25" type="text" class="input" name="mp3_voicetrack" value="http://" /><br />
Add Track On <input type="radio" name="mp3_voice_track_add" value="first" checked="yes" />First <input type="radio" name="mp3_voice_track_add" value="last" />Last</div>
<div class="form-grp"><input type="checkbox" name="mp3_info_view" checked="checked" /><div class="label">View Taged File Info?</div>
<center><input type="submit" name="submit" value="Edit Tags" /></center></div>
</form>
<?php
}
?>
<div class="bmenu"><center>Total <font color="red"><?php echo intval((count(scandir($default_mp3_directory))+count(scandir("voicetracks")))-4); ?></font> Mp3 Taged</center></div></div>
<div class="footer"><center> &copy; <?php echo date('Y'); ?>All Rights Reserved</center></div>
<p></p><center>Developed By: <a href="http://vishesh.tk">Vishesh Grewal<br /></a>
</body>
</html>