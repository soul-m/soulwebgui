<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10.11.17
 * Time: 17:05
 */

abstract class CustomEnum
{
    /**
     * @var array
     */
    private $_constants = null;

    /**
     * @var string
     */
    private $_name;

    /**
     * Konstruktor soll nicht öffentlich aufgerufen werden.
     */
    private function __construct()
    {
        $rc = new ReflectionClass($this);
        $this->_constants = $rc->getConstants();
    }

    /**
     * Quasi der Konstruktor.
     *
     * @param string $method
     * @param mixed $args
     * @return CustomEnum
     */
    public static function __callstatic($method, $args)
    {
        $class = get_called_class();
        $enum = new $class();

        return $enum->_set($method);
    }

    public function __call($method, $args)
    {
        $class = get_called_class();
        $enum = new $class();

        return $enum->_set($method);
    }

    public function ordinal()
    {
        return $this->_constants[$this->_status];
    }

    public function name()
    {
        return (string) $this->_status;
    }

    public static function values()
    {
        $list = array();
        $class = get_called_class();
        $enum = new $class();

        foreach ($enum->_constants as $name => $ordinal) {
            $list[] = $enum->$name();
        }
        return $list;
    }

    public function __toString()
    {
        return $this->name();
    }

    private function _set($index)
    {
        $this->_status = strtoupper($index);
        if (!isset($this->_constants[$this->_status])) {
            throw new UnexpectedValueException($this->_status . ' is not a valid value');
        }
        return $this;
    }

    public function equals(CustomEnum $enum)
    {
        return $enum->_status == $this->_status && get_class($enum) == get_class($this);
    }
}
