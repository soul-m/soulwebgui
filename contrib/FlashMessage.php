<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10.11.17
 * Time: 17:03
 */
require_once "CustomEnum.php";


class msg extends CustomEnum
{
    const DANGER = 1;
    const WARNING= 2;
    const INFO = 3;
    const SUCCESS = 4;
}

class Message{

    public $type;
    public $title;
    public $message;
    public $class;

    function __construct($type=msg::DANGER, $title="", $message="")
    {
        if($type >= 1 && $type <= 4){
            $this->type = $type;
            $this->class = $this->identify_class($type);
        }

        if($title){
            $this->title = $title;
        }

        if($message){
            $this->message = $message;
        }
    }

    private function identify_class($type){
        switch($type){

            case 1:
                return "error-msg";
            break;

            case 2:
                return "warning-msg";
            break;


            case 3:
                return "info-msg";
            break;

            case 4:
                return "success-msg";
            break;

        }
    }

    public function __toString()
    {
        return $this->generateHtml()."";
    }

    public function generateHtml(){
        return <<<EOT
<div class="$this->class">
    <b>$this->title</b><br>
    $this->message
</div>
EOT;
    }
}


class FlashMessage
{

    public $msg_list = [];

    public function __toString()
    {
        return $this->generateHtml()."";
    }

    public function addMessage($type=msg::DANGER, $tile="", $message=""){
        $msg = new Message($type, $tile, $message);
        array_push($this->msg_list, $msg);
    }

    public function generateHtml(){
        return implode("", $this->msg_list)."";
    }




}