<?php


ob_start();
include 'header.php';
include 'define.php';
header('Content-Type:text/html');
session_start();
function clean_array($arr)
{
for($i=0;$i<count($arr);$i++)
{
if(empty($arr[$i])) unset($arr[$i]);
}
return array_values($arr);
}
function filter_post($data){
$data = strip_tags($data);
// Fix &entity\n;
$data = str_replace(array('&amp;','&lt;','&gt;'), array('&amp;amp;','&amp;lt;','&amp;gt;'), $data);
$data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
$data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
$data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');
// Remove any attribute starting with "on" or xmlns
$data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);
// Remove javascript: and vbscript: protocols
$data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
$data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
$data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);
// Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);
// Remove namespaced elements (we do not need them)
$data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);
do
{
// Remove really unwanted tags
$old_data = $data;
$data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
}
while ($old_data !== $data);
// we are done...
return htmlspecialchars(str_replace(';',null,$data),ENT_QUOTES);
}

$name  = filter_post(@$_POST['name']);
$msg  = filter_post(@$_POST['msg']);
if(empty($name) || empty($msg))
{
$rand = rand(0,999);
$_SESSION['c'] = $rand;
echo '<div class="phdr">Write Post</div><form method="post">
<div class="list1">Name: <input type="text" name="name" /></div>
<div class="list1">Message: <textarea name="msg"></textarea></div>
<div class="list1"><center><b> '.intval($_SESSION['c']).'</b></center></div>
<div class="list1">Enter Above Digits: <input type="text" name="captcha" value="" /></div>
<div class="list1"><input type="submit" value="Message" /></div>
</form>';
}
else
{
if((intval($_POST['captcha'])==intval($_SESSION['c'])) && (!in_array(strtolower($name),$DisAllowedNames) && strtolower($name)!=$admin_name) && (ctype_alpha($name)))
{
if(file_exists('msgx.dat'))
{
$msgx = @fread(fopen('msgx.dat','r'),filesize('msgx.dat'));
$count = count(clean_array(explode("|||",$msgx)));
}
else
{ $count = 0; }
echo '<div class="phdr">Posted</div><div class="gmenu">Message has been posted <a href="guestbook.php"><b>Back</b></a></div>';
file_put_contents('msgx.dat',$name.'|*|'.$msg.'|*|'.$count.'|||',FILE_APPEND);
}
else
{
if(intval($_POST['captcha'])!=intval($_SESSION['c']))
echo '<div class="rmenu">Invalid Captcha <a href="guestbook.php"><b>Back</b></a></div>';
else echo '<div class="rmenu">Disallowed name</a></div>';
}
}
ob_flush();
include 'footer.php';
?>