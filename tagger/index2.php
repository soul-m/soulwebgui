<?php


include 'header.php';
function delete_old_files()
{
global $default_mp3_directory;
$hdir1 = scandir($default_mp3_directory);
unset($hdir1[0]);
unset($hdir1[1]);
if(count($hdir1)>0)
{
foreach($hdir1 as $file)
{
if(filemtime($default_mp3_directory."/".$file)<time()-3600)
{
unlink($default_mp3_directory."/".$file);
}
}
}
}
function friendly_size($size,$round=2)
{
$sizes=array(' Byts',' Kb',' Mb',' Gb',' Tb');
$total=count($sizes)-1;
for($i=0;$size>1024 && $i<$total;$i++){
$size/=1024;
}
return round($size,$round).$sizes[$i];
}
$default_mp3_directory =  "datas/";
delete_old_files();

include 'define.php';
if(isset($_POST['submit'])){
$mp3_filepath = $_POST['mp3_filepath'];
$mp3_filename = $_POST['mp3_filename'];
$mp3_songname = $_POST['mp3_songname'];
$mp3_comment = $_POST['mp3_comment'];
$mp3_band = $_POST['mp3_band'];
$mp3_publisher = $_POST['mp3_publisher'];
$mp3_composer = $_POST['mp3_composer'];
$mp3_original_artist = $_POST['mp3_original_artist'];
$mp3_copyright = $_POST['mp3_copyright'];
$mp3_encoded_by = $_POST['mp3_encoded_by'];
$mp3_user_defined = $_POST['mp3_user_defined'];
$mp3_wxxx = $_POST['mp3_wxxx'];
$mp3_artist = $_POST['mp3_artist'];
$mp3_album = $_POST['mp3_album'];
$mp3_year = $_POST['mp3_year'];
$mp3_genre = $_POST['mp3_genre'];
$mp3_track = $_POST['track'];

if(filter_var($mp3_filepath,FILTER_VALIDATE_URL)){
if($mp3_filename!="")
{
$mp3_filename = str_replace(DIRECTORY_SEPARATOR,"-X-",$mp3_filename);
if(strtolower(@end(@explode(".",basename($mp3_filepath))))!="mp3"){
exit("<br />URL must have a .mp3 extension !");
}
if(strtolower(@end(@explode(".",basename($mp3_filename))))!="mp3"){
exit("<br />Filename must have a .mp3 extension !");
}
$sname = $default_mp3_directory.$mp3_filename;
if((file_exists($sname)) || (file_exists("voicetracks/".$mp3_filename)))
{
$extension = @end(@explode('.',$sname));
$mp3_filename = str_replace(".".$extension,"_".rand(0,999).".".$extension,$mp3_filename);
}
$sname = $default_mp3_directory.$mp3_filename;
$j_sname = $default_mp3_directory.$mp3_filename;
if(@$_POST['mp3_voice_track']=="on")
{
$sname = "voicetracks/".$mp3_filename;
}
if(copy($mp3_filepath,$j_sname)){
if(@$_POST['mp3_voice_track']=="on" && $_POST['mp3_voicetrack']!='http://')
{
if(copy($_POST['mp3_voicetrack'],"temp_joins/".$mp3_filename))
{
include 'function.joinmp3.php';
$place = $_POST['mp3_voice_track_add'];
if($place=="first")
{
$FilenamesIn[] = "temp_joins/".$mp3_filename;
$FilenamesIn[] = $j_sname;
}
else
{
$FilenamesIn[] = $j_sname;
$FilenamesIn[] = "temp_joins/".$mp3_filename;
}
if (CombineMultipleMP3sTo($sname, $FilenamesIn)) {
unlink("temp_joins/".$mp3_filename);
unlink($default_mp3_directory."/".$mp3_filename);
rename("voicetracks/".$mp3_filename,$default_mp3_directory."/".$mp3_filename);
}
}
else echo '<div class="rmenu">Unable to upload Voice Track</div><br />';
}
$sname = $default_mp3_directory."/".$mp3_filename;
$size = friendly_size(filesize($sname));
$mp3_tagformat = 'UTF-8';
require_once('getid3/getid3.php');
$mp3_handler = new getID3;
$mp3_handler->setOption(array('encoding'=>$mp3_tagformat));
require_once('getid3/write.php');
$mp3_writter = new getid3_writetags;
$mp3_writter->filename       = $sname;
$mp3_writter->tagformats     = array('id3v1', 'id3v2.3');
$mp3_writter->overwrite_tags = true;
$mp3_writter->tag_encoding   = $mp3_tagformat;
$mp3_writter->remove_other_tags = true;
$mp3_data['title'][]   = $mp3_songname;
$mp3_data['artist'][]  = $mp3_artist;
$mp3_data['album'][]   = $mp3_album;
$mp3_data['year'][]    = $mp3_year;
$mp3_data['genre'][]   = $mp3_genre;
$mp3_data['comment'][] = $mp3_comment;
$mp3_data['publisher'][] = $mp3_publisher;
$mp3_data['composer'][] = $mp3_composer;
$mp3_data['band'][] = $mp3_band;
$mp3_data['commercial_information'][] = $mp3_wxxx;
$mp3_data['url_user'][] = $mp3_user_defined;
$mp3_data['track'][] = $mp3_track;
$mp3_data['original_artist'][] = $mp3_original_artist;
$mp3_data['copyright_message'][] = $mp3_copyright;
$mp3_data['encoded_by'][] = $mp3_encoded_by;

$album_art = @end(@explode('/',$_POST['album_art']));
$album_path = "temp_albumarts/".$album_art;
$album_ext = @end(@explode('.',$album_art));
if(in_array($album_ext,array("png","jpeg","gif","jpg")) && (copy($_POST['album_art'],$album_path)))
{
if($album_ext=="jpeg" || $album_ext=="jpg") $type = "image/jpeg";
if($album_ext=="png") $type="image/png";
if($album_ext=="gif") $type="image/png";
$mp3_data['attached_picture'][0]['data'] = file_get_contents($album_path);
$mp3_data['attached_picture'][0]['picturetypeid'] = $type;
$mp3_data['attached_picture'][0]['description'] = $album_art;
$mp3_data['attached_picture'][0]['mime'] = $type;
}
else
{
echo '<div class="rmenu">Incompartible image !</div>';
}

$mp3_writter->tag_data = $mp3_data;

if($mp3_writter->WriteTags()) {
$id3 = new getID3;
$info = $id3->analyze($sname);
file_put_contents('count.dat',$total_tagged+1);
echo '<center><div class="phdr"><b>MP3 Taged Notice</b></div></center><center><a class="fmenu" href="'.$sname.'"><img src="download.png"/></a></center>
<br>Size: <font color="blue">( '.$size.' )</font></center></div>
<div class="vheader">  <audio controls>
  <source src="'.$url.'/'.$sname.'"type="audio/mpeg">
This Script Is Developed By <a href="http://vishesh.tk"/>Vishesh Grewal</audio></div>
   <center><div class="phdr"><b>Direct Download Url</b></div><div align="center" class="list2">
<input size="25" type="text" class="input" name="mp3_filename" value="'.$url.'/'.$sname.'"></div></center><div class="rmenu">
<font color="red"><b>Warning:</b></font> Download Link Is AVAILABLE only for 1 hour</div>
';
if(@$_POST['mp3_info_view']=="on")
{
echo '<div class="phdr"><b>Mp3 Tag Information</b>
</div>';
if(!empty($_POST['album_art']))echo '<div class="list2"><center><img class="gmenu" src="cover.php?type='.$type.'&p='.basename($sname).'&img='.$album_art.'" width="200" height="200" alt="art">
</center></div>';
echo '<div class="list1">Title: '.$info['id3v1']['title'].'</div><div class="list2">Artist: '.$info['id3v1']['artist'].'</div>
<div class="list1">Album: '.$info['id3v1']['album'].'</div>
<div class="list2">Comment: '.$info['id3v1']['comment'].'</div><div class="list1">Year: '.$info['id3v1']['year'].'</div>
<div class="list2">Duration: '.$info['playtime_string'].'</div>';
}
}
else{
echo"<br />Failed to write tags!<br>".implode("<br /><br />",$mp3_writter->errors);
}
}
else{echo '<div class="rmenu">Unable to copy file.</div>';}
}
else{echo '<div class="rmenu">Empty filename.</div>';}
}
else{echo '<div class="rmenu">Invalid FilePath.</div>';}
}
else{
?>
<center><div class="vheader"><b><?php echo $sitename; ?></b></div>  <div class="phdr"><b>Mp3 Tag Editor</b></div></center>
<form method="post" action="" enctype="multipart/form-data">
<div class="list1"><b>&bull; Mp3 url</b><br /><input size="25" type="text" class="input" name="mp3_filepath" value="http://" /></div>
<div class="list2"><b>&bull; Album art/Cover</b><br /><input size="25" type="text" class="input" name="album_art" /><br/><a href="/upload">Upload AlbumArt</a></div>

<div class="list1"><b>&bull; Filename</b><br /><input size="25" type="text" class="input" name="mp3_filename" value=".mp3" /> </div>
<div class="list2"><b>&bull; Song name/title</b><br /><input size="25" type="text" class="input" name="mp3_songname" value="" /></div>
<div class="list1"><b>&bull; Album</b><br /><input size="25" type="text" class="input" name="mp3_album" value="<?php echo $default_album ; ?>" /> </div>
<div class="list2"><b>&bull; Artist(s)</b><br /><input size="25" type="text" class="input" name="mp3_artist" value="<?php echo $default_artist ; ?>" /></div>
<div class="list1"><b>&bull; Comment</b><br /><input size="25" type="text" class="input" name="mp3_comment" value="<?php echo $default_comment ; ?>" /></div>
<div class="list2"><b>&bull; Band</b><br /><input size="25" type="text" class="input" name="mp3_band" value="<?php echo $sitename; ?>" /></div>
<div class="list1"><b>&bull; Publisher</b><br /><input size="25" type="text" class="input" name="mp3_publisher" value="<?php echo $url; ?>" /></div>
<div class="list2"><b>&bull; Composer</b><br /><input size="25" type="text" class="input" name="mp3_composer" value="<?php echo $sitename; ?>" /></div>
<div class="list1"><b>&bull; Original Artist</b><br /><input size="25" type="text" class="input" name="mp3_original_artist" value="<?php echo $sitename; ?>" /></div>
<div class="list2"><b>&bull; Copyright</b><br /><input size="25" type="text" class="input" name="mp3_copyright" value="<?php echo $sitename; ?>" /></div>
<div class="list1"><b>&bull; Encoded By</b><br /><input size="25" type="text" class="input" name="mp3_encoded_by" value="<?php echo $sitename; ?>" /></div>
<div class="list2"><b>&bull; User Defined Url</b><br /><input size="25" type="text" class="input" name="mp3_user_defined" value="<?php echo $url; ?>" /></div>
<div class="list1"><b>&bull; Commercial Url</b><br /><input size="25" type="text" class="input" name="mp3_wxxx" value="<?php echo $url; ?>" /></div>
<div class="list2"><b>&bull; Track Number</b><br /><select name="track" class="input"><?php for($i=0;$i<=5;$i++) echo '<option value="'.$i.'">'.$i.'</option>'; ?></select></div>
<div class="list1"><b>&bull; Released Year</b><br /><select class="input" name="mp3_year"><?php for($d=date('Y');$d>=1950;$d--) echo '<option value="'.$d.'">'.$d.'</option>'; ?> </select></div>
<div class="list2"><b>&bull; Genre</b><br /><select name="mp3_genre" class="input">
<option value="<?php echo $default_genre ; ?>"><?php echo $default_genre ; ?></option>

<option value="Haryanvi">Haryana</option>
<option value="Punjabi">Punjab</option>
<option value="Bollywood">Bollywood</option>
<option value="Bihar">Bihar</option>
<option value="Bhojpuri">Bhojpuri</option>
<option value="Tamil">Tamil</option>
<option value="Bangla">Bangla</option>
<option value="Rajasthani">Rajasthan</option>
<option value="Marathi">Marathi</option>
<option value="Local">Local Area</option>
<option value="Instrumental PoP">Instrumental PoP</option>
<option value="Jazz">Jazz</option>
<option value="Metal">Metal</option>
<option value="Oldies">Rock</option>
<option value="PoP">PoP</option>
<option value="PoP-Folk">PoP-Folk</option>
<option value="Porn Groove">Porn Groove</option>
<option value="R&amp;B">R&amp;B</option>
<option value="Symphony">Symphony</option>
<option value="Slow Rock">Slow Rock</option>
<option value="Symphonic Rock">Symphonic Rock</option>
<option value="Sound Track">Sound Track</option>
<option value="Other">Other</option>
</select></div>
<div class="phdr"><input type="checkbox" name="mp3_voice_track" /><b>Add Voice Track</b> (Optional)</div>
<div class="list1"><b>&bull; Voice Track Url</b> [<a href="http://fromtexttospeech.com/"><b>Make Track</b></a>]<br /><input size="25" type="text" class="input" name="mp3_voicetrack" value="http://" /><br />
Add Track On <input type="radio" name="mp3_voice_track_add" value="first" checked="yes" />First <input type="radio" name="mp3_voice_track_add" value="last" />Last</div>
<div class="gmenu"><input type="checkbox" name="mp3_info_view" checked="checked" />View Taged File Info?<br />
<center><input size="15" type="submit" name="submit" value="Edit Tags" /></center></div>
</form>
<div class="phdr"><b>Menu</b></div>
<div class="list1"><b>&bull; <a href="data.php">Last 1h Tagged Files</a></b> (<font color="red"><b><?php echo intval((count(scandir($default_mp3_directory))+count(scandir("voicetracks")))-4); ?></b></font>)</div>

<div class="list2"><b>&bull; <a href="upload/data.php">Uploaded AlbumArt</a></b></div>
<div class="list2"><b>&bull; <a href="guestbook.php">Give Feedback</a></b></div>
<div class="list1"><b>&bull; <a href="code.php">Make Your Tag Editor</a></b></div>
<div class="list2"><b>&bull; <a href="http://facebook.com/hdharyana">Contact With Developer</a></b></div>
<div class="list1"><b>&bull; <a href="http://hrhost.cf">Get Free Hosting</a></b></div>
<div class="list2"><b>&bull; <a href="http://github.com/vkaygrewal">Get This Script Free</a></b></div>
<div class="list1"><b>&bull; <a href="http://facebook./sharer.php?u=http://<?=$_SERVER['SERVER_NAME'];?>">Share On FaceBook</a></b></div>
<div class="list2"><b>&bull; <a href="http://facebook.com/hdharyana">Get Pro Verion Of Mp3 Tag Editor V4 Only in Rs.500</a></b></div>
<?php
}
include 'footer.php';
?>
