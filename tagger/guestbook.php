<?php

ob_start();
header('Content-Type:text/html');

include 'header.php';

$perpage = 5;
$page = @$_GET['page'];
$page = isset($page) && $page!=0?$page:1;

function clean_array($arr)
{
for($i=0;$i<count($arr);$i++)
{
if(empty($arr[$i])) unset($arr[$i]);
}
return array_values($arr);
}
function paging($input,$page,$show_per_page,$return_type="array",$counter=0,$index=0,$num_pages=0,$limit_number=3,$limit_page=3,$admin=0)
{
$count = count($input);
$start = ($page-1)*$show_per_page;
$end = $start+$show_per_page;
if($return_type=="array")
{

if($start<0 || $count<=$start)
{

return false;
}
else
{
if($count<=$end)
{
return array_slice($input,$start);
}
else
{
return array_slice($input,$start,$end-$start);
}
}
} // end of array type
else
{
$page_offset = ( $page - 1) * $limit_page ;
$limit_number_start = $page - ceil ( $limit_number / 2);
$limit_number_end = ceil ( $page + $limit_number / 2) - 1;
if ( $limit_number_start < 1) $limit_number_start = 1;
$dif = ($limit_number_end - $limit_number_start);
if($dif<$limit_number) $limit_number_end = $limit_number_end+($limit_number-($dif+1));
if($limit_number_end>$num_pages) $limit_number_end = $num_pages;
$dif = ($limit_number_end - $limit_number_start);
if( $limit_number_start<1) $limit_number_start = 1;
$tot_pages = round($counter/$show_per_page);
global $sort;
for($i=$limit_number_start;$i<=$limit_number_end;$i++)
{
if($page!=$i)
$output = '<a class="pg" href="guestbook.php?page='.$i.'">'.$i.'</a> ';
else $output = '<td class="pg">'.$i.'</td> ';
echo $output;
}
}
}
echo '<div class="phdr">Guestbook</div><b><center><div class="gmenu"><a href="write.php">Write a Post</a></div></center></b>';
if(file_exists('msgx.dat'))
{
$msgx = fread(fopen('msgx.dat','r'),filesize('msgx.dat'));
$msg_h = clean_array(explode("|||",$msgx));
$msg_h = array_reverse($msg_h);
$count = count($msg_h);
$last_page = ceil($count/$perpage);
if(($page<=$last_page) && ($page>0))
{
$msg_h = paging($msg_h,$page,$perpage);
foreach($msg_h as $in_h)
{
if(!empty($in_h))
{
$read_in_h = explode('|*|',$in_h);
$usr_name = $read_in_h[0];
$usr_msg = $read_in_h[1];
echo '<div class="list1"><b>'.$usr_name.':</b> '.$usr_msg.'</div>';
}
}
}
else echo '<div class="rmenu">Invalid Page</div>';
}
else echo '<div class="rmenu">No Posts Found</div>';
echo '<div class="gmenu">';
echo @paging("n",$page,$perpage,"paging",$count,"n",$last_page,3,1);
echo '</div>';

include 'footer.php';
ob_flush();
?>