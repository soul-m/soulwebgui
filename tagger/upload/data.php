<?php

$sort = @$_GET['sort'];
$page = @$_GET['page'];
$sort = isset($sort) && in_array($sort,array(0,1))?$sort:1;
$page = isset($page) && $page!=0?$page:1;
$per_page = 5;
$dir = "uploads/";

include 'header.php';


function friendly_size($size,$round=2)
{
$sizes=array(' Bytes',' Kb',' Mb',' Gb',' Tb');
$total=count($sizes)-1;
for($i=0;$size>1024 && $i<$total;$i++){
$size/=1024;
}
return round($size,$round).$sizes[$i];
}
if($sort==1)
{
$handle = opendir($dir);
function datecmp($a,$b)
{
if($a==$b)
{
return 0;
}
return ($a<$b)?-1:1;
}
$h = array();
while(false !== ($run = readdir($handle)))
{
if($run!='.' && $run!='..')
{
$h[] = array(filemtime($dir.'/'.$run),$run);
}
}
closedir($handle);
usort($h,"datecmp");
$h = array_reverse($h);
}
else {
$h = scandir($dir);
unset($h[0]);
unset($h[1]);
}
function paging($input,$page,$show_per_page,$return_type="array",$counter=0,$index=0,$num_pages=0,$limit_number=3,$limit_page=3,$admin=0)
{
$count = count($input);
$start = ($page-1)*$show_per_page;
$end = $start+$show_per_page;
if($return_type=="array")
{

if($start<0 || $count<=$start)
{

return false;
}
else
{
if($count<=$end)
{
return array_slice($input,$start);
}
else
{
return array_slice($input,$start,$end-$start);
}
}
} // end of array type
else
{
$page_offset = ( $page - 1) * $limit_page ;
$limit_number_start = $page - ceil ( $limit_number / 2);
$limit_number_end = ceil ( $page + $limit_number / 2) - 1;
if ( $limit_number_start < 1) $limit_number_start = 1;
$dif = ($limit_number_end - $limit_number_start);
if($dif<$limit_number) $limit_number_end = $limit_number_end+($limit_number-($dif+1));
if($limit_number_end>$num_pages) $limit_number_end = $num_pages;
$dif = ($limit_number_end - $limit_number_start);
if( $limit_number_start<1) $limit_number_start = 1;
$tot_pages = round($counter/$show_per_page);
global $sort;
for($i=$limit_number_start;$i<=$limit_number_end;$i++)
{
if($page!=$i)
$output = '<a class="pg" href="data.php?page='.$i.'&sort='.$sort.'">'.$i.'</a> ';
else $output = '<td class="pg">'.$i.'</td> ';
echo $output;
}
}
}
$count = count($h);
$last_page = ceil($count/$per_page);
$h = paging($h,$page,$per_page);
echo '<div class="phdr"><b>User Uploaded Files</b></div>';
echo $sort==1?'<div class="gmenu">
<center><a href="data.php?page=1&sort=0"><b>Sort by A-Z</b></a>
</center></div>':'<div class="gmenu"><center><a href="data.php?page=1&sort=1"><b>Sort by Newest</b></a></center></div>';
if($count>0)
{
foreach($h as $file)
{
$r_file = $sort==1?$file[1]:$file;
echo '<div class="list2">&#187; <a href="'.$dir.''.$r_file.'">'.$r_file.'</a> ('.friendly_size(filesize($dir.$r_file)).') 
<br/>
';
}
echo '<div class="gmenu">';
echo paging($dir,$page,$per_page,"paging",$count,$dir,$last_page,3,1);
echo '</div>';
}
else
echo '<div class="rmenu">Files Not Found</a></div>';
include 'footer.php';
?>