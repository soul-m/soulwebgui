<?php
//  +------------------------------------------------------------------------+
//  | O!MPD, Copyright � 2015-2019 Artur Sierzant                            |
//  | http://www.ompd.pl                                                     |
//  |                                                                        |
//  |                                                                        |
//  | netjukebox, Copyright � 2001-2012 Willem Bartels                       |
//  |                                                                        |
//  | http://www.netjukebox.nl                                               |
//  | http://forum.netjukebox.nl                                             |
//  |                                                                        |
//  | This program is free software: you can redistribute it and/or modify   |
//  | it under the terms of the GNU General Public License as published by   |
//  | the Free Software Foundation, either version 3 of the License, or      |
//  | (at your option) any later version.                                    |
//  |                                                                        |
//  | This program is distributed in the hope that it will be useful,        |
//  | but WITHOUT ANY WARRANTY; without even the implied warranty of         |
//  | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
//  | GNU General Public License for more details.                           |
//  |                                                                        |
//  | You should have received a copy of the GNU General Public License      |
//  | along with this program.  If not, see <http://www.gnu.org/licenses/>.  |
//  +------------------------------------------------------------------------+




//  +------------------------------------------------------------------------+
//  | users.php                                                              |
//  +------------------------------------------------------------------------+
require_once('include/initialize.inc.php');
$cfg['menu'] = 'config';

$action		= getpost('action');
$user_id	= getpost('user_id');

if		($action == '')						home();



elseif	($action == 'online')				online();


else	message(__FILE__, __LINE__, 'error', '[b]Unsupported input value for[/b][br]action');
exit();

//  +------------------------------------------------------------------------+
//  | Check admin acount                                                     |
//  +------------------------------------------------------------------------+
function checkAdminAcount($user_id) {
	global $db;
	$query = mysqli_query($db,'SELECT user_id 
		FROM user 
		WHERE user_id != ' . (int) $user_id . '
		AND access_admin');
	$user = mysqli_fetch_assoc($query);
	if ($user['user_id'] == '') return false;
	else						return true;
}




//  +------------------------------------------------------------------------+
//  | Online                                                                 |
//  +------------------------------------------------------------------------+
function online() {
	global $cfg, $db;
	authenticate('access_admin');
		
	// formattedNavigator
	$nav			= array();
	$nav['name'][]	= 'Configuration';
	$nav['url'][]	= 'config.php';
	$nav['name'][]	= 'Poweroff';
	require_once('include/header.inc.php');	
?>
<table cellspacing="0" cellpadding="0" class="border">
<tr class="header">
</tr>
<tr class="line"><td colspan="13"></td></tr>
</table>
<br>

<div class="buttons">
<span><a onclick="return confirm('Are you sure you want to Poweroff ?')" href="index.php?action=poweroff">Poweroff</a></span>
<span><a onclick="return confirm('Are you sure you want to reboot?')" href="index.php?action=reboot">reboot</a></span>

</div>

<?php
	require_once('include/footer.inc.php');
}

?>
