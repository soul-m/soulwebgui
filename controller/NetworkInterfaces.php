<?php

    require_once "ConfController.php";
    require_once __DIR__."/../Settings.php";

    class NetworkInterfaces extends ConfController {

        public $template_name = "templates/network_interfaces.html";
        public $module_name = "network_interfaces";

        public function get_path(){
            return "/".shortcodes\joinPaths(Settings::$fs_path, "etc/network/interfaces");
        }

        protected function get_context_data(){
            if($this->comment){
                $this->context["status"] = "Mit anderem W-LAN verbunden (W-LAN Accesspoint ist ausgeschaltet)";
            }else{
                $this->context["status"] = "W-LAN Access Point ist aktiviert";
            }
            return $this->context;
        }

        public function post(){

            if($this->get_default("toggle") && !$this->get_default("ssid", false) && !$this->get_default("password", false)){
                syslog(LOG_DEBUG, "Network Interfaces entry is ". $this->comment);
                $this->toggle();
            }

            if(!$this->get_default("toggle", false) && ($this->get_default("ssid", false) || $this->get_default("password", false))) {

                if ($this->get_default("ssid", false)) {
                    $this->change_ssid();
                }

                if ($this->get_default("password", false)) {
                    $this->change_password();
                }
                $this->view->request["msg"]->addMessage(msg::SUCCESS, "Netzwerk", "Wifi SSID und Wifi Passwort wurden erfolgreich geändert");
            }
            $this->save();

            shell_exec("sudo service create_ap stop");
            shell_exec("sudo systemctl disable create_ap");
	    shell_exec("sudo service networking restart");
        }

        public function change_ssid(){
            $this->new_entry = $this->edit_param("wpa-ssid", $this->get_default("ssid"));
        }

        public function change_password(){
            $this->new_entry = $this->edit_param("wpa-psk", $this->get_default("password"));
        }

        public function edit_param($key, $value){
            return preg_replace('/(#?'.$key.'\s+)(.*)(.*)/', '$1'.$value.'$3', $this->new_entry);
        }

        public function toggle(){
            if(!$this->comment){
                $this->remove_block_comment();
                $this->view->request["msg"]->addMessage(msg::SUCCESS, "Netzwerk", "Die WLAN Einstellungen wurden aktiviert.");
            }else{
                $this->add_block_comment();
                $this->view->request["msg"]->addMessage(msg::SUCCESS, "Netzwerk", "Die WLAN Einstellungen wurden deaktiviert.");
            }
        }

    }
?>
