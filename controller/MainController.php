<?php

require_once "./contrib/Shortcodes.php";

    class MainController{

        public $template_name = "templates/none.html";
        public $module_name;
        protected $context = [];

        public function __construct($view)
        {
            $this->view = $view;

            // If attribute module_name not set create uiqid
            if(empty($this->module_name)){
                $this->module_name = uniqid();
            }
            // add module name to render context for this module
            $this->context = [
                "module_name"=>$this->module_name
            ];

        }
        /*
         * Get POST value of key or a default value
         * @param $key  Key of Post
         * @param $default Default result if key not in post available
         * return Mixed
         */
        protected function get_default($key, $default=""){
            if(isset($_POST[$key])){
                if(!empty($_POST[$key])){
                    return $_POST[$key];
                }
            }
            return $default;
        }

        protected function get_context_data(){
            //Method to render local context
            return $this->context;
        }

        protected function getModule(){
            //load module template
            $content = file_get_contents($this->template_name);
            // render template with local context
            return shortcodes\render($content, $this->get_context_data());
        }

        public function dispatch($request){
            // dispatch method call
            if($request["method"] == "get"){
                // Call "get" method when HTTP Method type is GET
                return $this->get();
            }elseif ($request["method"] == "post"){
                // Call "post" method when HTTP Method type is POST
                return $this->post();
            }
        }

        public function get(){
            // default get action
            // return html of the rendert module template
            return $this->getModule();
        }

        public function post(){
            throw new Exception("please implement post method");
        }

    }
?>