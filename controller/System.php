<?php

require_once "MainController.php";
require_once "SystemUpdate.php";


class System extends MainController {
    public $module_name = "system";
    public $template_name = "templates/system.html";

    public function __construct(){

        $args=func_get_args();
        call_user_func_array(array('parent',__FUNCTION__),$args);

        $this->system_update = new SystemUpdate();
    }

    public function get_context_data()
    {
        $context = parent::get_context_data();

        $context["update_content"] = $this->generate_update_content();

        $context["space_content"] = $this->generate_space_content();
        return $context;
    }

    public function post(){
        $update_commit = $this->get_default("update_to_commit", false);
        $update_tag = $this->get_default("update_to_tag", false);

        if($update_commit != false){
            $goto = $update_commit;
        }

        if($update_tag != false){
            $goto = $update_tag;
        }
        $this->system_update->do_pull(Settings::$git_remote_name);
        $this->system_update->do_reset($goto);

        $this->system_update = new SystemUpdate();
        echo "<script>window.location.href(true);</script>";
    }

    public function generate_space_content()
    {
        $free_bytes = disk_free_space(Settings::$main_hdd);
        $total_bytes =  disk_total_space( Settings::$main_hdd );
        
        # $percent_free = floor(($total_bytes/100*($total_bytes-$free_bytes)));
        $percent_free = (1-($free_bytes/$total_bytes))*100;

        $tempval=floatval($percent_free);
        $chopped=substr($tempval,0,8);
        $percent_free=number_format(round($chopped, 3),3);

        $total = $this->formatBytes($total_bytes);

        $used = $this->formatBytes($total_bytes-$free_bytes);
        $free = $this->formatBytes($free_bytes);

        return <<<EOT
                <div class='outer-space-bar'>
                    <span class='spacebar-marker outer-space-min-size-marker'>0 B</span>
                    <div class='inner-space-bar' style='width: $percent_free%'>
                        <span class='spacebar-marker inner-space-size-marker'> $used </span>
                    </div>
                    <span class='spacebar-marker outer-space-max-size-marker'> $total </span>
                </div>
                <br>
                Freier Speicherplatz für internen Speicher: <br>
                $free MB frei von $total <br> ($used in Benutzung)
EOT;
    }

    public function formatBytes($bytes, $precision = 2){
        $units = array('B', 'KB', 'MB', 'GB', 'TB');


        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        $bytes /= pow(1024, $pow);
        // $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }


    public function generate_update_content(){

        function get_button($name, $text, $value){
            return "<button name='$name' value='$value' type='submit'>$text</button>";
        }

        if(strpos($this->system_update->current["branch_name"], 'release') !== ""){
            $current_tag = $this->system_update->get_tag();
            $tag_list = $this->system_update->get_remote_tags();
            $new_tag = $this->system_update->compare_tags($current_tag, $tag_list);

            $notice = "Aktuelle Version $current_tag<br>";

            if($current_tag == $new_tag){
                return $notice."Es ist keine neuere Version verfügbar.";
            }else{
                return $notice."<i style='color:green;' class='fa fa-check'></i> Es ist eine neue Version verfügbar. ($new_tag)<br>".get_button("update_to_tag", "Update zu $new_tag", $new_tag);
            }

        }else{

            $remote_commit = $this->system_update->get_remote_commit(Settings::$git_remote_name, $this->system_update->current["branch_name"]);
            $notice = "<b>Entwickler Modus</b><br>Aktuelle Version ".$this->system_update->current['commit_uuid']."<br>";

            if($this->system_update->current['commit_uuid'] != $remote_commit){

                $notice .= get_button("update_to_commit", "Gehe zu ".$remote_commit , $remote_commit);

            }else{
                $notice .= "Nichts zu tun";
            }

            return $notice;
        }

    }

    public function clean_version($version){
        $re = '/v(\d\.?)*/';

        preg_match_all($re, $version, $matches, PREG_SET_ORDER, 0);
        return $matches[0][0];
    }

    public function get_current_tag(){
        $tag = shell_exec("git describe --abbrev=0 --tags");
        return $this->clean_version($tag);
    }

}
?>