<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 10.11.17
 * Time: 18:56
 */

require_once "MainController.php";

class ConfController extends MainController
{

    public function __construct($view)
    {
        // Load entry frm path (defined under get_path)
        $this->raw_entry = $this->get_entry();
        //Enthält den orginal eintrag
        $this->entry = $this->raw_entry[0];
        // Enthält einen (möglicherweiße) modifizierten eintrag
        $this->new_entry = $this->raw_entry[0];
        // Enthällt boolean ob der eintrag ein/auskommentiert ist
        $this->comment = $this->raw_entry[1];

        parent::__construct($view);
    }

    public function save()
    {
        // Lädt orginal datei nocheinmal
        $content = $this->get_file_content();

        // Sucht nach den mustern für die begrenzung
        preg_match_all('/#\s*start-automatic-generation-'.$this->module_name.'(.*)\n#\s*end-automatic-generation-'.$this->module_name.'/s', $content, $matches);

        // Create new entry
        $new_data = "# start-automatic-generation-".$this->module_name;
        $new_data .= $this->new_entry . "\n";
        $new_data .= "# end-automatic-generation-".$this->module_name;

        // Replace old entry with new entry
        $data = preg_replace('/#\s*start-automatic-generation-'.$this->module_name.'(.*)\n#\s*end-automatic-generation-'.$this->module_name.'/s', $new_data, $content);

        // overwrite orginal file with new content
        $this->write_content_to_file($data);

        $entry = $this->get_entry();
        $this->entry = $entry[0];
        $this->new_entry = $entry[0];
        $this->comment = $entry[1];
    }

    public function get(){
        if(is_array($this->raw_entry)){
            return $this->getModule();
        }else{
            $this->view->request["msg"]->addMessage(msg::WARNING, $this->module_name, "Module cant display while config does not exists");
        }
    }

    public function get_path(){
        throw new Exception("please Implement this Method");
    }

    public function add_block_comment(){
        $result = [];
        // Split by linebreak
        foreach(explode("\n", $this->new_entry) as $key => $line){
            // comment single line
            $new_line = "#".$line;
            //add line
            if($new_line != ""){
                array_push($result, $new_line);
            }
        }
        // Write result to new entry
        $this->new_entry = implode("\n", $result);
    }

    public function remove_block_comment(){
        $result = [];
        // split by linebreak
        foreach(explode("\n", $this->new_entry) as $key => $line){

            // remove comment by line
            $new_line = preg_replace("/#(.*)/", "$1", $line);
            array_push($result, $new_line);
        }
        // write result to new entry
        $this->new_entry = implode("\n", $result);
    }


    private function get_file_content() {
        // load content from config
        $content = file_get_contents($this->get_path());
        return $content;
    }


    private function write_content_to_file($content) {
        // write content to config
        file_put_contents($this->get_path(), $content);
    }

    protected function get_entry() {
        // Extract entry from configuration file
        $content = $this->get_file_content();
        preg_match_all('/#\s*start-automatic-generation-'.$this->module_name.'(.*)\n#\s*end-automatic-generation-'.$this->module_name.'/s', $content, $matches);

        if(isset($matches[1])){

            if(isset($matches[1][0])){
                $entry = $matches[1][0];
                preg_match('/\s*(#)\s*/', $entry, $comment_match);
                $comment = false;
                if(count($comment_match) < 1){
                    $comment = true;
                }
                return [$entry, $comment];
            }
            return false;
        }
        return false;
    }
}