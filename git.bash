#!/usr/bin/env bash


export GIT_SSH_COMMAND="ssh -o LogLevel=quiet -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"
git config core.safecrlf false
# chmod -R 777 /var/www/soulm/*

RED='\033[0;31m'
NC='\033[0m' # No Color
GREEN='\033[0;32m'

function show_help {
    echo "  Usage: ./build.bash [OPTIONS] "
    echo ""
    echo "GNU long option		        Meaning"
    echo "--help			            Show this message"
    echo "--remote <<RemoteName>        Definiert den Remote name (Default ist 'origin')"
    echo "--branch <<BranchName>        Definiert den Branch zu dem Informationen verarbeitet werden sollen"
    echo "--get-tag                     Gibt den namen des derzeitigen Tags aus"
    echo "--get-commit                  Gibt uuid des aktuellen commits zurück"
    echo ""
    echo "Die folgenden befehle können optional mit --remote <<RemoteName>> ergänzt werden"
    echo "--get-branch                  Gibt den derzeitigen namen des branch aus"
    echo "--get-remote-tags                    Liste von allen Tags"

    echo "--go-to-tag <<tag>>           Wechsle zu Tag oder Commit (no default)"
    echo ""
    echo "--branch <<branchname>>       Auf welchem branch (default release) für alle folgenden commands mit nutzen"
    echo "--change-branch               Wechsle den Branch"
    echo "--get-last-commit             UUID of last commit"
}

function set_cache {
    # $1 NAME
    # $2 VALUE
    # Setzt den Cache
    echo $2
}

function check_cache {
    # $1 NAME
    # return true wenn neuer cache erzeugt werden muss
    echo "true"
}

function get_cache {
    # $1 NAME
    # gibt cache zurück
    echo .
}

CMD_HELP=false

CMD_RESET=false
CMD_BRANCH=false
CMD_REMOTE=false

CMD_GET_TAG=false
CMD_GET_COMMIT=false
CMD_GET_BRANCH=false
CMD_GET_REMOTE_COMMIT=false
CMD_GET_REMOTE_TAGS=false
CMD_FETCH=false
CMD_PULL=false

CMD_GET_COMMIT=false
CMD_CHANGE_BRANCH=false
CMD_GOTO_TAG=false
CMD_GET_LAST_COMMIT=false

POSITIONAL=()


while [[ $# -gt 0 ]]
do
key="$1"

case ${key} in

    --branch)
    CMD_BRANCH="$2"
    shift
    shift
    ;;

    --remote)
    CMD_REMOTE="$2"
    shift
    shift
    ;;

    --get-tag)
    CMD_GET_TAG=true
    #shift # past argument
    shift # past value
    ;;

    --get-commit)
    CMD_GET_COMMIT=true
    #shift # past argument
    shift # past value
    ;;

    --get-branch)
    CMD_GET_BRANCH=true
    shift
    ;;

    # Kombi mit --remote möglich
    --get-remote-tags)
    CMD_GET_REMOTE_TAGS=true
    #shift # past argument
    shift # past value
    ;;

    # Kombi mit --remote möglich
    --get-remote-commit)
    CMD_GET_REMOTE_COMMIT=true
    #shift # past argument
    shift # past value
    ;;

    --reset)
    CMD_RESET="$2"
    shift # past argument
    shift # past value
    ;;

    --pull)
    CMD_PULL=true
    #shift # past argument
    shift # past value
    ;;

    --fetch)
    CMD_FETCH=true
    #shift # past argument
    shift # past value
    ;;

    --checkout)
    CMD_CHECKOUT="$2"
    shift # past argument
    shift # past value
    ;;

    --help)
    CMD_HELP=true
    # shift # past argument
    shift
    ;;

    *)    # unknown option

    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done

# set -- "${POSITIONAL[@]}" # restore positional parameters

ARG_STRING=""
ARG_FILE=""

if [ "${CMD_HELP}" = true ] ; then
    show_help
    exit
fi

# Basics
if [ "${CMD_REMOTE}" != false ] ; then
    REMOTE_URL=$(git config --get remote.${CMD_REMOTE}.url)
    REMOTE_NAME=${CMD_REMOTE}
else
    REMOTE_URL=$(git config --get remote.origin.url)
    REMOTE_NAME=origin
fi

if [ "${CMD_BRANCH}" != false ] ; then
    BRANCH_NAME=${CMD_BRANCH}
else
    BRANCH_NAME=$(git rev-parse --abbrev-ref HEAD)
fi

if [ "${CMD_GET_TAG}" != false ]; then
    commit_uuid=$(git describe --abbrev=0 --tags)
    echo $commit_uuid
    exit;
fi

if [ "${CMD_GET_COMMIT}" != false ]; then
    commit_uuid=$(git rev-parse HEAD)
    echo $commit_uuid
    exit;
fi

if [ "${CMD_GET_BRANCH}" != false ]; then
    branch_name=$(git rev-parse --abbrev-ref HEAD)
    echo $branch_name
    exit;
fi

if [ "${CMD_GET_REMOTE_TAGS}" != false ] ; then

    cache_name="${REMOTE_NAME}_GET_TAGS"

    if [ $(check_cache cache_name) = "true" ] ; then


        tags=( )
        # Load all Tags from remote
        uncleaned_tag=$(git ls-remote --quiet --tags --refs ${REMOTE_NAME} | awk '{print $2}') >/dev/null 2>&1

        IFS=$'\n'       # make newlines the only separator

        # Split tag information by line
        for line in ${uncleaned_tag}
        do
            first="$( cut -d '/' -f 1 <<< "$line" )";
            # Use only lines that begins with refs
            if [ ${first} = "refs" ] ; then
                # Add tagname to list
                tags=("${tags[@]}" "${line##*/}")
            fi
        done
        # Return String with multiple Tags seperated by ';'
        set_cache $cache_name $(printf "%s;" "${tags[@]}" | cut -d ";" -f 1-${#tags[@]})
    else
        get_cache $cache_name

    fi

    exit;
fi

if [ "${CMD_GET_REMOTE_COMMIT}" != false ]; then
    commit_uuid=$(git ls-remote ${REMOTE_URL} "refs/heads/${BRANCH_NAME}" | head -1 | cut -f 1)
    echo $commit_uuid
    exit;
fi

if [ "${CMD_FETCH}" != false ]; then
    echo $(git fetch ${REMOTE_NAME} ${BRANCH_NAME})
    exit;
fi


if [ "${CMD_PULL}" != false ]; then
    echo $(git pull)

    if [ "${CMD_BRANCH}" != false ] ; then
        echo $(git pull -t ${REMOTE_NAME} ${BRANCH_NAME})
    else
        echo $(git pull ${REMOTE_NAME})
        echo $(git pull -t ${REMOTE_NAME})
    fi
    echo $(git pull)
    exit;
fi

if [ "${CMD_CHECKOUT}" != false ]; then
    echo $(git checkout ${CMD_CHECKOUT})
    exit;
fi

if [ "${CMD_RESET}" != false ]; then
    echo $(git reset --hard "${CMD_RESET}")
    exit;
fi
