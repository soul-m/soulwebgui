<?php
/**
 * Created by PhpStorm.
 * User: stefan
 * Date: 23.10.17
 * Time: 12:48
 */

require_once "./contrib/View.php";
require_once "./contrib/Shortcodes.php";
require_once "./controller/NetworkInterfaces.php";
require_once "./controller/Arm.php";
require_once "./controller/Fstab.php";
//require_once "./controller/SystemCtl.php";
require_once "./controller/UpmpdcliQobuz.php";
require_once "./controller/UpmpdcliHighresaudio.php";
require_once "./controller/UpmpdcliTidal.php";
//require_once "./controller/soulTidal.php";
require_once "./controller/System.php";


class DashboardView extends View
{

    function __construct()
    {
        // Ruft den übergeortneten Constructor auf
        parent::__construct();
        // Liste mit allen modulen
        $this->modules = [
            new NetworkInterfaces($this),
            new Fstab($this),
            new Arm($this),
            //new SystemCtl($this),
            new UpmpdcliQobuz($this),
            new UpmpdcliTidal($this),
//            new soulTidal($this),
            new UpmpdcliHighresaudio($this),
            new System($this)
        ];

    }

    public function get($force=false){
        // Load render Context
        $context = $this->get_context_data();

        $modules = "";
        // Collect all modules
        for($i = 0; $i < count($this->modules); $i++){
            if($force){
                // Collect module templates
                $modules .= $this->modules[$i]->get();
            }else{
                // Run dispatch in every module
                $modules .= $this->modules[$i]->dispatch($this->request);
            }
        }

        // Add result (html) to context
        $this->context["modules"] = $modules;
        // run rendering of template variables
        return shortcodes\render(shortcodes\get_template("templates/main.html"), $this->context);
    }

    public function post(){
        //check if action was set
        if(isset($_GET["action"])){
            if(!empty($_GET["action"])){
                // Suche das modul, das den gleichen module_name als attribut hat wie der get parameter action
                foreach($this->modules as $key => $module){
                    if($module->module_name == $_GET["action"]){
                        // Call dispatch of this module
                        $module->dispatch($this->request);
                    }
                }
            }

        }
        // Return blank html page with all modules
        return $this->get(true);
    }



}
